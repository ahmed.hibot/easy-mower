package fr.upem

object ListUtils {

  def flattenList[A](list : List[Option[A]]) : Option[List[A]] = list match {
    case list if list != Nil && !list.contains(None) => Option(list.flatten)
    case _ => None
  }
}

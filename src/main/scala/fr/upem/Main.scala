package fr.upem


object Main extends App{
  import Parsers.parseFile
  import Utils._

  override def main(args: Array[String]): Unit = args match {
    case Array(filename) => parseFile(filename) match {
      case Some(configuration) => run(configuration) match {
        case Some(positions) => positions.reverse.foreach(position => println(position.coordinates._1 + " " + position.coordinates._2 + " " + position.direction))
        case _ => println("Incorrect configuration")
      }
      case _ => println("Incorrect file")
    }
    case _ => println("Missing argument : filename")
  }
}

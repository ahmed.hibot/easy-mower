package fr.upem


object Utils {
  import scala.io.Source
  import Classes._
  import ListUtils._


  def checkCoordinates(coordinates: (Int, Int), limits: (Int, Int)) = {
    val (x, y) = coordinates
    val (xLimit, yLimit) = limits
    x >= 0 && y >= 0 && x <= xLimit && y <= yLimit
  }


  def updateCoordinates(position: Position, limits: (Int, Int), takken: List[(Int, Int)]): Position = {
    val (x, y) = position.coordinates
    val newCoordinates = position.direction match {
      case North => (x, limits._2.min(y + 1))
      case South => (x, 0.max(y - 1))
      case East => (limits._1.min(x + 1), y)
      case West => (0.max(x - 1), y)
    }

    if (takken.contains(newCoordinates)) position else Position(newCoordinates, position.direction)
  }

  def turnLeft(direction: Direction): Direction = direction match {
    case East => North
    case West => South
    case North => West
    case South => East
  }

  def turnRight(direction: Direction): Direction = direction match {
    case East => South
    case West => North
    case North => East
    case South => West
  }

  def applyAction(position: Position, action: Action, limits: (Int, Int), takken: List[(Int, Int)]): Position = action match {
    case Left => Position(position.coordinates, turnLeft(position.direction))
    case Right => Position(position.coordinates, turnRight(position.direction))
    case Advance => updateCoordinates(position, limits, takken)
  }

  def applyActions(position: Position, actions: List[Action], limits: (Int, Int), takken: List[(Int, Int)]): Option[Position] = if (takken.contains(position.coordinates)) None else actions match {
    case Nil => None
    case action :: Nil => Some(applyAction(position, action, limits, takken))
    case action :: rest => applyActions(applyAction(position, action, limits, takken), rest, limits, takken)
  }

  def runLawnmowers(lawnmowers: List[Lawnmower], limits: (Int, Int), takken: List[Position]): List[Option[Position]] = lawnmowers match {
    case Nil => takken match {
      case Nil => Nil
      case list => list.map(position => Some(position))
    }
    case lawnmower :: rest => applyActions(lawnmower.position, lawnmower.actions, limits, takken.map(position => position.coordinates)) match {
      case Some(position) => runLawnmowers(rest, limits, position :: takken)
      case _ => None :: Nil
    }
  }

  def run(configuration: Configuration): Option[List[Position]] = flattenList(runLawnmowers(configuration.lawnmowers, configuration.grassland.limits, Nil))

  def getLines(filename: String): Option[List[String]] = {
    try {
      Some(Source.fromFile(filename).getLines.toList)
    } catch {
      case e: Exception => None
    }
  }
}
